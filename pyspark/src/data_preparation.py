#!/usr/bin/env python
# coding: utf-8

"""
A pyspark job to prepare data for KeySet computation.

$SPARK_HOME/bin/spark-submit pyspark/src/data_preparation.py  --start-date 2022-03-01 --end-date 2022-03-02 --output-path pa_keyset
"""

import argparse
from datetime import datetime

from pyspark.sql import SparkSession
from pyspark.sql import functions as sf

country_query = """
SELECT DISTINCT
  country_code as country
FROM
  wmf.geoeditors_public_monthly
WHERE country_code != '--'
"""

pv_query = """
SELECT DISTINCT
  pageview_info['page_title'] as page_title,
  page_id,
  pageview_info['project'] as project
  , geocoded_data['country'] as country
  , actor_signature
FROM wmf.pageview_actor
WHERE
  is_pageview 
  AND namespace_id = 0
  AND concat_ws('-', year, month, day) BETWEEN '{start_date}' AND '{end_date}'
  AND pageview_info['page_title'] IS NOT NULL
  AND page_id IS NOT NULL
  AND pageview_info['project'] IS NOT NULL
  AND geocoded_data['country'] IS NOT NULL
  AND actor_signature IS NOT NULL
  AND lower(pageview_info['page_title']) != 'nan'
"""


def format_dt(dt):
    """
    Strip the leading 0 from the day/month of a ISO 8601 string.

    :param dt: a YYYY-MM-DD string representing a date.
    """
    return datetime.strptime(dt, "%Y-%m-%d").strftime("%Y-%-m-%-d")


def make_argparse():
    parser = argparse.ArgumentParser(description="Prepare data for KeySet computation")
    parser.add_argument(
        "--output-path",
        metavar="output_path",
        type=str,
        help="Output path",
        default="pa_key_set-2",
    )
    parser.add_argument(
        "--start-date",
        metavar="start_date",
        type=str,
        help="Start date YYYY-MM-DD",
        default=datetime.today().strftime("%Y-%-m-%-d"),
    )
    parser.add_argument(
        "--end-date",
        metavar="end_date",
        type=str,
        help="End date YYYY-MM-DD",
        default=datetime.today().strftime("%Y-%m-%d"),
    )
    return parser


def main(args):
    spark = SparkSession.builder.getOrCreate()

    start_date = format_dt(args.start_date)
    end_date = format_dt(args.end_date)
    article_df = (
        spark.sql(pv_query.format(start_date=start_date, end_date=end_date))
        .select("project", "page_id")
        .distinct()
    )
    country_df = spark.sql(country_query)
    # Cache `country_df` and materialize it before the crossJoin starts.
    country_df.cache()
    country_df.collect()

    key_df = country_df.crossJoin(article_df)

    # TODO: error handling with a reasonable fallback in case we run
    # with spark2 and/or dynamic allocation.
    num_cores = int(spark.conf.get("spark.executor.cores"))
    num_executors = int(spark.conf.get("spark.executor.instances"))
    key_df.repartition(num_executors * num_cores)

    key_df.write.parquet(args.output_path)

    spark.stop()


if __name__ == "__main__":
    parser = make_argparse()
    args = parser.parse_args()
    main(args)
