SHELL := /bin/bash

original_repo := $(shell echo ${HOME})/stat-spark3
project := ${original_repo}
spark_home := ${project}/spark3
venv := tumult
stat_host=${hostname}.eqiad.wmnet

venv:
	./scripts/pack_python_env.sh

install: venv

uninstall:
	rm -rf ${HOME}/.local
	rm ${HOME}/.conda/environments.txt
	rm -rf ${HOME}/.conda/env/${venv}
	rm ${project}/venv-conda.tar.gz

update: uninstall install

jupyter:
	./scripts/start_spark3_notebook.sh

