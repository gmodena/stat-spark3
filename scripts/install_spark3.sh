#!/bin/bash

# enable external downloads
export http_proxy=http://webproxy.eqiad.wmnet:8080
export https_proxy=http://webproxy.eqiad.wmnet:8080

# download and untar spark3.2.1
wget https://dlcdn.apache.org/spark/spark-3.0.3/spark-3.0.3-bin-hadoop2.7.tgz
tar xvzf spark-3.0.3-bin-hadoop2.7.tgz

# clean-up
rm spark-3.0.3-bin-hadoop2.7.tgz
mv spark-3.0.3-bin-hadoop2.7/conf/log4j.properties.template spark-3.0.3-bin-hadoop2.7/conf/log4j.properties

printf "\nIf you are running this command on its own, run './scripts/start_spark3_notebook.sh' now to start a notebook\n\n'"
