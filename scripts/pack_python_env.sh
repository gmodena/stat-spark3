#!/usr/bin/env bash
#
# WARNING: by using this script you are deviating from recommended WMF Analytics
# vendoring practices. No support is provided.
#
# TODO(gmodena): the rationale here is that some upstream versions clash
# with base conda ones. We can possibly streamline this, but I'd rather
# prioritize testing the tumult package in a relatively "stable" env.
# TODO(gmodena): in the future we might not be allowed to install packages
# directly on stat boxes. There's tools we could tap into to pack env
# on build hosts and deploy, but it's premature optimization for now.
#
# This script creates and packages a conda environment 
# with tumult's dependencies installed at pinned versions.
# We use it to start jupyter notebooks,
# and to ship tumult dependencies to spark workers.
# 
# Run with: 
# $ TUMULT_USER=<username> TUMULT_PASSWORD=<password> ./pack-python-env.sh
#
# The resulting packed environment will be available at ./venv-conda.tar.gz
# 
# The environment will be create under ${HOME}/.conda.
# It can be activated with 
# $ source /usr/lib/anaconda-wmf/bin/conda-activate-stacked tumult
# 
source /usr/lib/anaconda-wmf/bin/conda-activate-stacked

export http_proxy=http://webproxy.eqiad.wmnet:8080
export https_proxy=http://webproxy.eqiad.wmnet:8080
venv=tumult
python_version=3.7.4

conda create -n ${venv} python=${python_version}
conda activate ${venv}
pip install -v --extra-index-url https://${TUMULT_USER}:${TUMULT_PASSWORD}@gitlab.com/api/v4/projects/17405343/packages/pypi/simple tmlt.analytics
conda install -y jupyter
conda pack --ignore-editable-packages -n tumult  -o venv-conda.tar.gz

source /usr/lib/anaconda-wmf/bin/conda-deactivate-stacked
